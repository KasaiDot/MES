modules['colorizePTWPriorities'] = {
	isEnabled: true,
	includes: [
		'/animelist/',
		'/mangalist/'
	],
	excludes: [],
	libs: [],
	run: function run() {
		console.log('running colorizePTWPriorities');
		//colors tags too?
		$("table tbody tr td:last-child:contains('High')").css("background-color","#FF6666"); //high = red
        $("table tbody tr td:last-child:contains('Medium')").css("background-color","#FFFF75"); //medium = yellow
        $("table tbody tr td:last-child:contains('Low')").css("background-color","#66FF66"); //low = green
	}
};