modules['endlessScrolling'] = {
	isEnabled: true,
	includes: [
		'/topanime.php',
		'/topmanga.php'
	],
	excludes: [],
	libs: [
		'https://dl.dropboxusercontent.com/u/30678687/web/jquery.infinitescroll.js'
	],
	run: function run() {
		console.log('running endlessScrolling');
		waitForLib();
		function waitForLib(){
		    if($.isFunction($.fn.infinitescroll)){
                console.log('infinitescroll is defined');
		        initEndlessScrolling();
		    }
		    else{
		    	console.log('infinitescroll not defined yet');
		        setTimeout(function(){
		            waitForLib();
		        },100);
		    }
		}
		
		function initEndlessScrolling(){
			if(window.location.pathname.indexOf('/topanime.php') >= 0){
	            $('#content').children().eq(1).infinitescroll({
	                nextSelector : ".spaceit a:contains('Next 30')",    
	                               // selector for the NEXT link (to page 2)
	                itemSelector : "#content div table",          
	                               // selector for all items you'll retrieve
	                path: function(pageNum){
	                    return '/topanime.php?type=&limit=' + ((pageNum - 1) * 30);
	                },
	                navSelector: ".spaceit",
	                loading: {
	                    msgText: "<em>Loading the next 30 anime...</em>"
	                 }
	            }, function() {
	            	$('a.hoverinfo_trigger').hoverIntent({
	                   sensitivity: 1, // number = sensitivity threshold (must be 1 or higher)
	                   interval: 200, // number = milliseconds for onMouseOver polling interval
	                   over: showInfo, // function = onMouseOver callback (required)
	                   timeout: 300, // number = milliseconds delay before onMouseOut
	                   out: hideInfo // function = onMouseOut callback (required)
	                });
	            });
	            
	        }
	        else if(window.location.pathname.indexOf('/topmanga.php') >= 0) {
	            
	            $('#content').children().eq(1).infinitescroll({
	                nextSelector : ".borderClass a:contains('Next 30'):last",    
	                               // selector for the NEXT link (to page 2)
	                itemSelector : "#content div table",          
	                               // selector for all items you'll retrieve
	                path: function(pageNum){
	                    return '/topmanga.php?type=&limit=' + ((pageNum - 1) * 30);
	                },
	                navSelector: ".borderClass:contains('Next 30'):last",
	                loading: {
	                    msgText: "<em>Loading the next 30 manga...</em>"
	                 }
	            }, function() {
	            	$('a.hoverinfo_trigger').hoverIntent({
	                   sensitivity: 1, // number = sensitivity threshold (must be 1 or higher)
	                   interval: 200, // number = milliseconds for onMouseOver polling interval
	                   over: showInfo, // function = onMouseOver callback (required)
	                   timeout: 300, // number = milliseconds delay before onMouseOut
	                   out: hideInfo // function = onMouseOut callback (required)
	                });
	            });
	            
	            var howCalculated = $("#content a:contains('How are these scores')")[0].outerHTML;
	            $("#content a:contains('How are these scores')").remove();
	            $('#content').prepend(howCalculated);
	            
	            $('#content').prepend('<br>');
	            
	            var currentScores = $("#content div > small:contains('Scores current')")[0].outerHTML;
	            $("#content div > small:contains('Scores current')").remove();
	            $('#content').prepend(currentScores);
	            
	            $('#content  > div > br').remove();
	           }
	      }
      }
};