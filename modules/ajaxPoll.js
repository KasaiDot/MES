modules['ajaxPoll'] = {
	isEnabled: true,
	includes: [
		'/forum/'
	],
	excludes: [],
	libs: [],
	run: function run() {
		console.log('running ajaxPoll');
		
    	var poll = $('.forum_boardrow1').eq(1);
    	var viewResultsUrl = $('.forum_boardrow1 div a:contains("View Results")').attr('href');

    	function replacePollWithResults(){
    		$.ajax({
			  url: viewResultsUrl,
			  cache: false
			})
			  .done(function( html ) {
			  	var pollResultsHTML = $(html).find('.forum_boardrow1').eq(1).html();
			  	poll.html(pollResultsHTML);
			  });
    	}

    	//ajaxify view results link
    	$('.forum_boardrow1 div a:contains("View Results")').click(function(event) {
    		event.preventDefault();
    		replacePollWithResults();
		});

		//ajaxify vote now form
		$("form input[name='submitPoll']").click(function(event){
			event.preventDefault();
    		event.stopPropagation(); //prevent other handlers from being notified, we ran first! >:)

			var data = $('#content form').eq(0).serializeArray();
			data.push({name: "submitPoll", value: 'Vote Now'});

			var settings = {
				type: "POST",
			    data: data, // serializes the form's elements.
			    success: function(data){
			    	//console.log('poll sent success');
			    }
			}
			$.ajax(settings).done(function(result){
				//user vote is not visible from ajax req for some reason
				replacePollWithResults();
				//setTimeout(replacePollWithResults, 10000);
			});

		});

	}
};