modules['liveSearch'] = {
	isEnabled: true,
	includes: [
		'all'
	],
	excludes: [
		'/animelist/',
		'/mangalist/'
	],
	libs: [
		'https://dl.dropboxusercontent.com/u/30678687/web/jquery.liveSearch.js'
	],
	run: function run() {
		console.log('running liveSearch');
		//only searches anime, add manga, etc. support based on selected
        waitForLib();
		function waitForLib(){
		    if($.isFunction($.fn.liveSearch)){
                console.log('liveSearch is defined');
                var searchUrl = $( "#topSearchValue option:selected" ).text().toLowerCase().replace(/ /g, '-');
                searchUrl = '/' + searchUrl + '.php?q=';
                //console.log("Search: " + searchUrl);
		        $('#topSearchText').liveSearch();
                
		    }
		    else{
		    	console.log('liveSearch not defined yet');
		        setTimeout(function(){
		            waitForLib();
		        },100);
		    }
		}
	}
};