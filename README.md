MES
===

MAL Enhancement Suite

Features:  
---
*   live search
*   colored PTW priorities  
*   full width  layout
*   edit details on show info pages open in overlay  
*   endless scrolling top anime/manga
*   WYSIWYG forum editor  
*   filter VA/people ists by shows seen  
*   ajax forum polls

Planned Features:  
---
*   night mode  (toggle)
*   community scores on PTW list  
*   english title default/toggle  
*   watch links to CrunchyRoll, Hulu, Aniplex, etc.  
*   endless scrolling forum threads
*   quick add shows from search results, top lists, etc.
*   custom css for non-list pages (background image, club themes)
*   decimal scores (cached) (already exists as a script?)
*   better recommendtions (scrape other recommendation site?)  
*   hide characters on incomplete shows by default
*   live list style editor
*   WYSIWYG comment, profile, club editor
