// ==UserScript==
// @name          MALES
// @author        ipodnerd
// @description   MAL Enhancement Suite
// @namespace     ipodnerd.byterevel.com
// @version       0.1
// @run-at        document-body
// @include       *://myanimelist.net/*
// @require       https://dl.dropboxusercontent.com/u/30678687/web/init.js
// @require       http://cdn.wysibb.com/js/jquery.wysibb.min.js
// ==Modules==
// @require       https://rawgit.com/JordekFluffball/MES/master/modules/colorizePTWPriorities.js
// @require       https://rawgit.com/JordekFluffball/MES/master/modules/voiceActorFilter.js
// @require       https://dl.dropboxusercontent.com/u/30678687/web/endlessScrolling.js
// @require       https://dl.dropboxusercontent.com/u/30678687/web/liveSearch.js
// @require       https://dl.dropboxusercontent.com/s/dmmfgyo22tbtlvc/ajaxPoll.js
// ==/Modules==
// ==Libraries==
// ==/Libraries==
// ==/UserScript==
//note: require's get  hard cached, eventually serve from the same directory

var linkSearch = window.document.createElement('link');
linkSearch.rel = 'stylesheet';
linkSearch.type = 'text/css';
linkSearch.href = 'https://dl.dropboxusercontent.com/u/30678687/web/jquery.liveSearch.css';
document.getElementsByTagName("head")[0].appendChild(linkSearch);

var linkSearch = window.document.createElement('link');
linkSearch.rel = 'stylesheet';
linkSearch.type = 'text/css';
linkSearch.href = 'https://dl.dropboxusercontent.com/u/30678687/web/styles.css';
document.getElementsByTagName("head")[0].appendChild(linkSearch);

var linkBB = window.document.createElement('link');
linkBB.rel = 'stylesheet';
linkBB.type = 'text/css';
linkBB.href = 'http://cdn.wysibb.com/css/default/wbbtheme.css';
document.getElementsByTagName("head")[0].appendChild(linkBB);



$(document).ready(function()
{
    initModules();
    
    //bbcode editor
    /*var scrollScript = document.createElement('script'); 
    scrollScript.src = 'http://cdn.wysibb.com/js/jquery.wysibb.min.js';
    scrollScript.type = 'text/javascript'; 
    document.body.appendChild(scrollScript);*/
    
    
    //add fancybox to edit details on anime page
    if(window.location.pathname.indexOf('/anime/') >= 0){
        var detailLink = $("td small a:contains('Edit Details')").attr('href');
        detailLink = detailLink + "&hideLayout=true";
        $("td small a:contains('Edit Details')").attr('href', detailLink);
        $("td small a:contains('Edit Details')").fancybox({'width':980,'height':'85%','autoScale':true,'autoDimensions':true,'transitionIn':'none','transitionOut':'none','type':'iframe'});
    }
    
    //timout bc scripts take time to load
    setTimeout(function(){
      if(window.location.pathname.indexOf('/forum/') >= 0){
          var wbbOpt = {
              buttons: "bold,italic,underline,strike,spoiler,quote,|,img,video,link,|,bullist,numlist,|,fontcolor,fontsize,|,justifycenter,justifyright,|,smilebox,|,removeFormat",
              allButtons: {
                video: {
                  transform: {
                    '<iframe src="http://www.youtube.com/embed/{SRC}" width="640" height="480" frameborder="0"></iframe>':'[yt]{SRC}[/yt]'
                  }
                },
                quote: {
                  transform: {
                    '<div class="quotetext"><!--quotesaid-->{SELTEXT}<!--quote--></div>':'[quote]{SELTEXT}[/quote]',
                    '<div class="quotetext"><strong>{AUTHOR} said:</strong><!--quotesaid--><br>{SELTEXT}<!--quote--></div>':'[quote={AUTHOR}]{SELTEXT}[/quote]'
                  }
                },
                spoiler: {
                   title: 'Insert Spoiler',
                   buttonText: 'spoiler',
                   transform: {
                       '<blockquote><strong>Spoiler:</strong><br>{SELTEXT}</blockquote>':'[spoiler]{SELTEXT}[/spoiler]'
                   }
                }
              },
              
              smileList: [
                        {title:'smile', img: '<img src="http://i.myniceprofile.com/573/57386.jpg" class="sm">', bbcode:":)"},
                        ]
            }

             $("#messageText").wysibb(wbbOpt);
          	$("textarea[name='msg_text']").wysibb(wbbOpt);
          $('table#dialog').css('width', '95%');
          $('#quickReply').parent().css('width', '100%');
                  
          
          
        }
    }, 700);
});
