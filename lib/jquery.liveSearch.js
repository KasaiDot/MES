/***
@title:
Live Search

@version:
2.0

@author:
Andreas Lagerkvist

@date:
2008-08-31

@url:
http://andreaslagerkvist.com/jquery/live-search/

@license:
http://creativecommons.org/licenses/by/3.0/

@copyright:
2008 Andreas Lagerkvist (andreaslagerkvist.com)

@requires:
jquery, jquery.liveSearch.css

@does:
Use this plug-in to turn a normal form-input in to a live ajax search widget. The plug-in displays any HTML you like in the results and the search-results are updated live as the user types.

@howto:
jQuery('#q').liveSearch({url: '/ajax/search.php?q='}); would add the live-search container next to the input#q element and fill it with the contents of /ajax/search.php?q=THE-INPUTS-VALUE onkeyup of the input.

@exampleHTML:
<form method="post" action="/search/">

	<p>
		<label>
			Enter search terms<br />
			<input type="text" name="q" />
		</label> <input type="submit" value="Go" />
	</p>

</form>

@exampleJS:
jQuery('#jquery-live-search-example input[name="q"]').liveSearch({url: Router.urlForModule('SearchResults') + '&q='});
***/
console.log("init liveSearch");
jQuery.fn.liveSearch = function (conf) {
	var config = jQuery.extend({
		id:				'jquery-live-search', 
		duration:		400, 
		typeDelay:		200,
		loadingClass:	'loading-notice', 
		onSlideUp:		function () {}, 
		uptadePosition:	false
	}, conf);

	var liveSearch	= jQuery('#' + config.id);

	// Create live-search if it doesn't exist
	if (!liveSearch.length) {
		liveSearch = jQuery('<div id="' + config.id + '"></div>')
						.appendTo(document.body)
						.hide()
						.slideUp(0);

		// Close live-search when clicking outside it
		jQuery(document.body).click(function(event) {
			var clicked = jQuery(event.target);

			if (!(clicked.is('#' + config.id) || clicked.parents('#' + config.id).length || clicked.is('input'))) {
				liveSearch.slideUp(config.duration, function () {
					config.onSlideUp();
				});
			}
		});
	}

	return this.each(function () {
		var input							= jQuery(this).attr('autocomplete', 'off');
		var liveSearchPaddingBorderHoriz	= parseInt(liveSearch.css('paddingLeft'), 10) + parseInt(liveSearch.css('paddingRight'), 10) + parseInt(liveSearch.css('borderLeftWidth'), 10) + parseInt(liveSearch.css('borderRightWidth'), 10);

		// Re calculates live search's position
		var repositionLiveSearch = function () {
			var tmpOffset	= input.offset();
			var inputDim	= {
				left:		tmpOffset.left, 
				top:		tmpOffset.top, 
				width:		input.outerWidth(), 
				height:		input.outerHeight()
			};

			inputDim.topPos		= inputDim.top + inputDim.height;
			inputDim.totalWidth	= inputDim.width - liveSearchPaddingBorderHoriz;

			liveSearch.css({
				position:	'absolute', 
				left:		inputDim.left + 'px', 
				top:		inputDim.topPos + 'px',
				width:		inputDim.totalWidth + 'px'
			});
		};

		// Shows live-search for this input
		var showLiveSearch = function () {
			// Always reposition the live-search every time it is shown
			// in case user has resized browser-window or zoomed in or whatever
			repositionLiveSearch();

			// We need to bind a resize-event every time live search is shown
			// so it resizes based on the correct input element
			$(window).unbind('resize', repositionLiveSearch);
			$(window).bind('resize', repositionLiveSearch);

			liveSearch.slideDown(config.duration);
		};

		// Hides live-search for this input
		var hideLiveSearch = function () {
			liveSearch.slideUp(config.duration, function () {
				config.onSlideUp();
			});
		};

		input
			// On focus, if the live-search is empty, perform an new search
			// If not, just slide it down. Only do this if there's something in the input
			.focus(function () {
				if (this.value !== '') {
					// Perform a new search if there are no search results
					if (liveSearch.html() == '') {
						this.lastValue = '';
						input.keyup();
						//console.log('no html content');
					}
					// If there are search results show live search
					else {
						input.keyup();
						// HACK: In case search field changes width onfocus
						//console.log('show prev live search results');
						//setTimeout(showLiveSearch, 1);
					}
				}
			})
			// Auto update live-search onkeyup
			.keyup(function () {
				console.log("search keyUp");

				var searchCat = $( "#topSearchValue option:selected" ).text().toLowerCase().replace(/ /g, '-');
				console.log(this.lastCat + ' ' + searchCat);
				var isNewCat = (this.lastCat != searchCat);
				this.lastCat = searchCat;

				if(searchCat != 'clubs' && searchCat != 'users'){

					// Don't update live-search if it's got the same value as last time
					if (this.value != this.lastValue || isNewCat == true) {
						input.addClass(config.loadingClass);

						var q = this.value;

						// Stop previous ajax-request
						if (this.timer) {
							clearTimeout(this.timer);
						}

						// Start a new ajax-request in X ms
						this.timer = setTimeout(function () {
							var searchUrl = $( "#topSearchValue option:selected" ).text().toLowerCase().replace(/ /g, '-');
							if(searchUrl == 'characters'){
								searchUrl = 'character';
							}
	                    	searchUrl = '/' + searchUrl + '.php?q=';
	                    	console.log(searchUrl + q);
							$.ajax({
									  url: searchUrl + q + '&o=3',
									  beforeSend: function( xhr ) {
									    //xhr.setRequestHeader("User-Agent","api-taiga-32864c09ef538453b4d8110734ee355b");
									  }
									})
									  .done(function( data ) {
									    input.removeClass(config.loadingClass);

										// Show live-search if results and search-term aren't empty
										if (data.length > 0 && q.length > 0) {
											console.log("Processing Data");
											/*var $data = $(data);
											$data.find('#header').remove(); 
											$data.find('#menu').remove();
											$data.find('#myanimelist').css("width","364px");
											alert($data.html());*/
											liveSearch.html($($.parseHTML(data)));
											$("#"+config.id + " #header").remove();
											$("#"+config.id + " #menu").remove();
											$("#"+config.id + " #headerSmall").remove();
											$("#"+config.id + " #roadblock").remove();
											$("#"+config.id + " #horiznav_nav").remove();
											$("#"+config.id + " form").remove();
											$("#"+config.id + " #rightcontentunder").remove();

											//remove headers
											$("#"+config.id + " h1:contains('Anime')").remove();
											$("#"+config.id + " h1:contains('Manga')").remove();
											$("#"+config.id + " h1:contains('Search')").remove();

											//characters
											$("#"+config.id + " tr td small:contains('Anime:')").remove();
											$("#"+config.id + " tr td small:contains('Manga:')").remove();

											//fansubs
											$("#"+config.id + " .spaceit:contains('Type in the name of a fansub')").remove();
											$("#"+config.id + " .spaceit:contains('find the group you')").remove();

											$("#"+config.id + " a:contains('edit')").remove();
											$("#"+config.id + " link").remove();
											$("#"+config.id + " .borderClass:first").remove();
											$("#"+config.id + " .spaceit:contains('read more')").remove();
											$("#"+config.id + " #content > div").css("padding","0px");
											$("#"+config.id + " #myanimelist").css("width","354px");
											//$($.parseHTML($data))
											//console.log($data.html());
											showLiveSearch();
										}
										else {
											hideLiveSearch();
										}
							});
						}, config.typeDelay);

						this.lastValue = this.value;
					}
				}
			});
	});
};
